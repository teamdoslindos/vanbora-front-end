# vanbora-front-end

Aplicação mobile realizada em Java para Android com o intuito de oferecer o gerenciamento de rotas e viagens para motoristas de van.
Para mais informações do projeto acesse: [vanbora](https://gitlab.com/teamdoslindos/vanbora)

## Instalação

### Windows


Instale o [Chocolatey](https://chocolatey.org/) (gerenciador de pacotes do windows)
```powershell
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
```


Instale o Android SDK
```powershell
choco install android-sdk -y
```

Instale os pacotes do Android SDK 29 que serão necessários
```powershell
"%ANDROID_HOME%\tools\bin\sdkmanager" "emulator" "platform-tools" "platforms;android-29" "build-tools;29.0.2" "extras;android;m2repository" "extras;google;m2repository"
```

Instale o Android Studio
Você pode instalar utilizando este [link](https://developer.android.com/studio).

## Rodando a aplicação no smartphone

``` bash
# install dependencies and run in debug mode
gradle installDebug
```

## Resolvendo problemas comuns

#### Erros de build

##### Signatures do not match previously installed version

```java
INSTALL_FAILED_UPDATE_INCOMPATIBLE: Package com.teamdoslindos.vanbora
signatures do not match previously installed version; ignoring!
```

**Por que ocorreu?** A versão da aplicação instalada no dispositivo não é compatível com as alterações que estão sendo aplicadas.

**Solução:** Apague o aplicativo Vanbora instalado no seu dispositivo e builde novamente.

##### No conected devices!
```java
com.android.builder.testing.api.DeviceException: No connected devices!
```
**Por que ocorreu?** Não foi encontrado nenhum dispositivo para instalar a aplicação.

**O jeito mais fácil/rápido para rodar a aplicação sem pesar no pc é no seu próprio celular, então as soluções serão focadas para esse caso.*

**Solução:** Conecte seu dispositivo em modo de [Depuração USB](https://www.nextpit.com.br/como-ativar-depuracao-usb-android).

