INSERT INTO public.address(
	id, street, neighborhood, city, "number", latitude, longitude)
	VALUES (1, 'rua a', 'satelite', 'sjc', '12', -1.0, -3.45);

INSERT INTO public.app_user(
	id, name, email, password, birth_date, phone, cpf, cnh, address_id)
	VALUES (1, 'joseph', 'a@a.com', 'a', now(), '123', '123', '123', 1);

INSERT INTO public.travel_group(
	id, name, leave, "return", description, address_id, driver_id)
	VALUES (1, 'viagem', now(), now(), 'muito bao', 1, 1);