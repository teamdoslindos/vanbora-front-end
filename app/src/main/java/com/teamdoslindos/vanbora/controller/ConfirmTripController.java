package com.teamdoslindos.vanbora.controller;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.ui.activity.ConfirmTripActivity;
import com.teamdoslindos.vanbora.ui.activity.LoginActivity;

public class ConfirmTripController {
    private RequestQueue mQueue;

    public void changeTripDepartureStatus(boolean isDeparture, Integer tripId, LinearLayout tela, Context context) {
        mQueue = Volley.newRequestQueue(context);
        String URL;
        if (isDeparture) {
            URL = ConfigUtils.API_HEROKU_BACK_END + "group/" + tripId + "/confirmDeparture/" + LoginActivity.user.getId();
        } else {
            URL = ConfigUtils.API_HEROKU_BACK_END + "group/" + tripId + "/confirmReturn/" + LoginActivity.user.getId();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("LOG_VOLLEY", response);
                Snackbar.make(tela, "Confirmação alterada com sucesso!",
                        Snackbar.LENGTH_SHORT)
                        .show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LOG_VOLLEY", error.toString());
                Snackbar.make(tela, "Algo deu errado! Tente novamente.",
                        Snackbar.LENGTH_SHORT)
                        .show();
            }
        }) {

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {

                    responseString = String.valueOf(response.statusCode);

                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };

        mQueue.add(stringRequest);
    }
}
