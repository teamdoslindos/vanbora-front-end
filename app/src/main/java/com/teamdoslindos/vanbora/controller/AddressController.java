package com.teamdoslindos.vanbora.controller;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.teamdoslindos.vanbora.config.ConfigUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class AddressController {
    private JSONObject jsonObjectAddress;

    public void getAddressFromTomTom(Context context, String address, CallBackAddress callBackAddress) throws UnsupportedEncodingException {
        RequestQueue mQueue = Volley.newRequestQueue(context);
        String param = URLEncoder.encode(address, "utf-8");
        String url = ConfigUtils.API_TOMTOM + param;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jsonInString = response.getJSONArray("results").get(0).toString();
                            jsonObjectAddress = new JSONObject(jsonInString);
                            JSONObject addressTemp = (JSONObject) jsonObjectAddress.get("address");
                            addressTemp.remove("extendedPostalCode");
                            jsonObjectAddress.put("address", addressTemp);
                            callBackAddress.callBackAddress(jsonObjectAddress);
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }
}
