package com.teamdoslindos.vanbora.controller;

import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.model.Van;
import com.teamdoslindos.vanbora.ui.activity.LoginActivity;
import com.teamdoslindos.vanbora.ui.activity.VanRegisterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class VanRegisterController {
    private static RequestQueue mQueue;
    private static VanRegisterActivity vanRegisterActivity = new VanRegisterActivity();

    public static void updateVan(LinearLayout tela, Context context, Van van) {
        mQueue = Volley.newRequestQueue(context);
        String url = "https://vanbora-back-end.herokuapp.com/vanbora/van";
        int method = Request.Method.POST;
        StringRequest postRequest = new StringRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Snackbar.make(tela, "Cadastrado com sucesso!",
                                Snackbar.LENGTH_SHORT).setActionTextColor(Color.GREEN)
                                .show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar.make(vanRegisterActivity.findViewById(R.id.telaVanRegister), "Algo deu errado! Por favor tente novamente.",
                                Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                                .show();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject vanJson = new JSONObject();
                try {
                    vanJson.put("id", van.getId());
                    vanJson.put("photo", null);
                    vanJson.put("plate", van.getPlate());
                    vanJson.put("color", van.getColor());
                    vanJson.put("quantityOfSeats", van.getQuantityOfSeats().toString());
                    vanJson.put("model", van.getModel());

                    JSONObject driver = new JSONObject();
                    driver.put("id", LoginActivity.user.getId());
                    vanJson.put("driver", driver);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String body = vanJson.toString();
                try {
                    return body.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        mQueue.add(postRequest);
    }
}
