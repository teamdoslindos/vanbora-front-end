package com.teamdoslindos.vanbora.controller;

import com.teamdoslindos.vanbora.model.User;

public interface CallBackLogin {

    void callBack(User user);
}
