package com.teamdoslindos.vanbora.controller;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.teamdoslindos.vanbora.model.Group;
import com.teamdoslindos.vanbora.model.Localization;
import com.teamdoslindos.vanbora.model.User;
import com.teamdoslindos.vanbora.model.Van;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ParseUtils {

    public static Group parseJsonGroup(JSONObject response) {
        Type Group = new TypeToken<Group>() {
        }.getType();
        return new Gson().fromJson(String.valueOf(response), Group);
    }

    public static User parseJsonUser(JSONObject response) {
        Type listType = new TypeToken<User>() {
        }.getType();
        User result = new Gson().fromJson(String.valueOf(response), listType);
        return result;
    }

    public static List<Group> parseJsonListGroup(JSONArray response) {
        Type listType = new TypeToken<ArrayList<Group>>() {
        }.getType();
        List<Group> result = new Gson().fromJson(String.valueOf(response), listType);
        return result;
    }

    public static List<Van> parseJsonListVan(JSONArray response) {
        Type listType = new TypeToken<ArrayList<Van>>() {
        }.getType();
        List<Van> result = new Gson().fromJson(String.valueOf(response), listType);
        return result;
    }

    public static List<Localization> parseJsonListLocalization(JSONObject response) throws JSONException {
        JSONArray jsonArray = response.getJSONArray("results");
        Type listType = new TypeToken<ArrayList<Localization>>() {
        }.getType();
        List<Localization> result = new Gson().fromJson(String.valueOf(jsonArray), listType);
        return result;
    }
}
