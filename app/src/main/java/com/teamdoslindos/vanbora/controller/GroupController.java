package com.teamdoslindos.vanbora.controller;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.model.Group;
import com.teamdoslindos.vanbora.model.Passenger;
import com.teamdoslindos.vanbora.ui.activity.GroupActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import static com.teamdoslindos.vanbora.controller.ParseUtils.parseJsonGroup;
import static com.teamdoslindos.vanbora.ui.activity.GroupActivity.allPassengers;
import static com.teamdoslindos.vanbora.ui.activity.GroupActivity.group;
import static com.teamdoslindos.vanbora.ui.activity.GroupActivity.passengersName;

public class GroupController {
    private RequestQueue mQueue;

    public void getGroupInfo(TextView mGroupInfo, Integer groupId, Context context) {
        mQueue = Volley.newRequestQueue(context);
        String url = ConfigUtils.API_HEROKU_BACK_END + "group/" + groupId;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            group = parseJsonGroup(response);
                            mGroupInfo.setText(String.format("%s \nSaída: %s\nVolta: %s",
                                    group.getName(),
                                    group.getArrivalTime(),
                                    group.getReturnTime()));

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    public void setToggleTrip(LinearLayout tela, Button button, Context context) {
        mQueue = Volley.newRequestQueue(context);
        button.setOnClickListener(view -> {
            String url = "https://vanbora-back-end.herokuapp.com/vanbora/group";
            StringRequest postRequest = new StringRequest(Request.Method.PATCH, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (group.isActive()) {
                                button.setText("Encerrar viagem");
                            } else {
                                button.setText("Iniciar viagem");
                            }
                            Snackbar.make(tela, "Viagem alterada com sucesso!",
                                    Snackbar.LENGTH_SHORT).setActionTextColor(Color.GREEN)
                                    .show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Snackbar.make(tela, "Algo deu errado! Tente novamente.",
                                    Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                                    .show();
                        }
                    }
            ) {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public byte[] getBody() throws AuthFailureError {
                    JSONObject json = new JSONObject();
                    try {
                        boolean newState = !group.isActive();
                        group.setActive(newState);
                        json.put("id", group.getId());
                        json.put("active", newState);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String body = json.toString();
                    return body.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };
            mQueue.add(postRequest);
        });
    }

    public void getPassengersFromGroup(LinearLayout tela, ArrayAdapter<Passenger> adapter,
                                       Integer groupId, Context context) {
        mQueue = Volley.newRequestQueue(context);
        String url = ConfigUtils.API_HEROKU_BACK_END + "group/" + groupId;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Group result = parseJsonGroup(response);
                        allPassengers.clear();
                        passengersName.clear();
                        for (Passenger passenger : result.getPassengers()) {
                            allPassengers.add(passenger);
                            passengersName.add(passenger.getUser().getName());
                        }
                        Log.e("Passengers", GroupActivity.passengers.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Snackbar.make(tela, "Algo deu errado! Tente novamente.",
                        Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                        .show();
            }
        });
        mQueue.add(request);
    }
}
