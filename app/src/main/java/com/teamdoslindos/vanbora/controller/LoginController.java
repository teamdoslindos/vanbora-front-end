package com.teamdoslindos.vanbora.controller;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.teamdoslindos.vanbora.config.ConfigUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import static com.teamdoslindos.vanbora.controller.ParseUtils.parseJsonUser;

public class LoginController {

    private RequestQueue mQueue;

    public void postUserLogin(Context context, String email, String password, CallBackLogin callBack) throws JSONException {
        mQueue = Volley.newRequestQueue(context);

        String url = ConfigUtils.API_HEROKU_BACK_END + "user/login";

        JSONObject body = createBody(email, password);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack.callBack(parseJsonUser(response));
                    }
                }, error -> {
            error.printStackTrace();
            callBack.callBack(null);
        });
        mQueue.add(request);
    }

    @NotNull
    private JSONObject createBody(String email, String password) throws JSONException {
        JSONObject body = new JSONObject();
        body.put("email", email);
        body.put("password", password);
        return body;
    }
}
