package com.teamdoslindos.vanbora.controller;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.ui.activity.GroupActivity;

import java.io.UnsupportedEncodingException;

public class EditEstimatedHoursController {
    private RequestQueue mQueue;

    public void changeHoursEstimatedOfPassenger(LinearLayout tela, Integer passengerId, String mRequestBody,
                                                Context context, boolean isDeparture, boolean isReturn) {
        mQueue = Volley.newRequestQueue(context);
        String URL = null;
        if (isDeparture) {
            URL = ConfigUtils.API_HEROKU_BACK_END + "group/" + GroupActivity.group.getId() + "/changeDepartureTime/" + passengerId;
        } else if (isReturn) {
            URL = ConfigUtils.API_HEROKU_BACK_END + "group/" + GroupActivity.group.getId() + "/changeReturnTime/" + passengerId;
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("LOG_VOLLEY", response);
                Snackbar.make(tela, "Alterado com sucesso!",
                        Snackbar.LENGTH_SHORT)
                        .show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LOG_VOLLEY", error.toString());
                Snackbar.make(tela, "Algo deu errado! Tente novamente.",
                        Snackbar.LENGTH_SHORT)
                        .show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {

                    responseString = String.valueOf(response.statusCode);

                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };

        mQueue.add(stringRequest);
    }

}
