package com.teamdoslindos.vanbora.controller;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.model.Group;
import com.teamdoslindos.vanbora.ui.activity.HomePassengerActivity;
import com.teamdoslindos.vanbora.ui.activity.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import static com.teamdoslindos.vanbora.controller.ParseUtils.parseJsonListGroup;

public class HomePassengerController {

    public static void getTripsFromPassenger(LinearLayout tela, Context context) {
        RequestQueue mQueue = Volley.newRequestQueue(context);
        String url = ConfigUtils.API_HEROKU_BACK_END + "group/byPassenger/" + LoginActivity.user.getId();

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<Group> result = parseJsonListGroup(response);
                        HomePassengerActivity.tripsList.clear();
                        HomePassengerActivity.tripsListString.clear();
                        HomePassengerActivity.tripsNames.clear();
                        for (Group group : result) {
                            HomePassengerActivity.tripsList.add(group);
                            HomePassengerActivity.tripsListString
                                    .add(group.toStringPassenger(LoginActivity.user.getId()));
                            HomePassengerActivity.tripsNames.add(group.getName());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Snackbar.make(tela, "Passageiro não possui nenhuma viagem!",
                        Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                        .show();
            }
        });
        mQueue.add(request);
    }

    public static void addPassengerToGroup(LinearLayout tela, Context context, String groupCode) throws JSONException {
        RequestQueue mQueue = Volley.newRequestQueue(context);
        String url = ConfigUtils.API_HEROKU_BACK_END + "group/" + groupCode + "/addPassenger/" + LoginActivity.user.getId();
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        getTripsFromPassenger(tela, context);
                        Snackbar.make(tela, "Viagem adicionada!",
                                Snackbar.LENGTH_SHORT).setActionTextColor(Color.GREEN)
                                .show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar.make(tela, "Algo deu errado! Por favor tente novamente.",
                                Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                                .show();
                    }
                }) {
        };
        mQueue.add(postRequest);
    }

}
