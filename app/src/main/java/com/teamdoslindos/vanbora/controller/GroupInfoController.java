package com.teamdoslindos.vanbora.controller;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.model.Group;
import com.teamdoslindos.vanbora.ui.activity.GroupActivityInfo;

import org.json.JSONObject;
import org.w3c.dom.Text;

import static com.teamdoslindos.vanbora.controller.ParseUtils.parseJsonGroup;

public class GroupInfoController {
    private RequestQueue mQueue;

    public void getGroupInfo(LinearLayout tela, TextView mCode, TextView mDestiny, TextView mAddress,
                             TextView mDate, TextView mIsGroupActive, int groupId, Context context) {
        mQueue = Volley.newRequestQueue(context);
        String url = ConfigUtils.API_HEROKU_BACK_END + "group/" + groupId;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Group group = parseJsonGroup(response);
                            mCode.setText("Código da viagem: " + group.getId());
                            mDestiny.setText(group.getName());
                            mAddress.setText(String.format("%s, %s - %s, %s",
                                    group.getAddress().getAddress().getStreetName(),
                                    group.getAddress().getAddress().getStreetNumber(),
                                    group.getAddress().getAddress().getMunicipalitySubdivision(),
                                    group.getAddress().getAddress().getMunicipality()));
                            mDate.setText(String.format("Ida: \nHora saída: %s \nHora de chegada: %s\nVolta: " +
                                            "\nHora saída: %s \nHora de chegada: %s",
                                    group.getOptimalStart(),
                                    group.getArrivalTime(),
                                    group.getReturnTime(),
                                    group.getOptimalEnd()));
                            mIsGroupActive.setText(group.isActive() ? "Viagem ativa: Sim" : "Viagem ativa: Não");

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Snackbar.make(tela, "Algo deu errado! Tente novamente.",
                        Snackbar.LENGTH_SHORT)
                        .show();
            }
        });
        mQueue.add(request);
    }
}
