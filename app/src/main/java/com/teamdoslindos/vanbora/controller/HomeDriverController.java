package com.teamdoslindos.vanbora.controller;

import android.content.Context;
import android.graphics.Color;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.model.Group;
import com.teamdoslindos.vanbora.model.Van;
import com.teamdoslindos.vanbora.ui.activity.HomeDriverActivity;
import com.teamdoslindos.vanbora.ui.activity.LoginActivity;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static com.teamdoslindos.vanbora.controller.ParseUtils.parseJsonListGroup;
import static com.teamdoslindos.vanbora.controller.ParseUtils.parseJsonListVan;

public class HomeDriverController {
    private RequestQueue mQueue;

    public void getTripsFromDriver(RelativeLayout tela, Context context) {
        mQueue = Volley.newRequestQueue(context);
        String url = ConfigUtils.API_HEROKU_BACK_END + "group/byDriver/" + LoginActivity.user.getId();

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<Group> result = parseJsonListGroup(response);
                        HomeDriverActivity.listGroups.clear();
                        HomeDriverActivity.listGroupsNames.clear();
                        for (Group group : result) {
                            HomeDriverActivity.listGroups.add(group);
                            HomeDriverActivity.listGroupsNames.add(group.getName());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Snackbar.make(tela, "Motorista não possui nenhuma viagem!",
                        Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                        .show();
            }
        });
        mQueue.add(request);
    }

    public void getVansFromDriver(RelativeLayout tela, Context context) {
        mQueue = Volley.newRequestQueue(context);
        String url = ConfigUtils.API_HEROKU_BACK_END + "van/byDriver/" + LoginActivity.user.getId();

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<Van> result = parseJsonListVan(response);
                        HomeDriverActivity.listVans.clear();
                        for (Van van : result) {
                            HomeDriverActivity.listVans.add(van);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Snackbar.make(tela, "Motorista não possui nenhuma van!",
                        Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                        .show();
            }
        });
        mQueue.add(request);
    }

}
