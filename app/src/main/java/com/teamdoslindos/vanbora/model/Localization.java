package com.teamdoslindos.vanbora.model;

import com.tomtom.online.sdk.search.location.AddressRanges;

public class Localization {

    private String type;
    private String id;
    private Double score;
    private Address address;
    private Position position;
    private ViewPort viewport;
    private AddressRanges addressRanges;

    public Localization() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Address getAddress() {
        return address;
    }

    public void setAdress(Address address) {
        this.address = address;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public ViewPort getViewport() {
        return viewport;
    }

    public void setViewport(ViewPort viewport) {
        this.viewport = viewport;
    }

    public AddressRanges getAddressRanges() {
        return addressRanges;
    }

    public void setAddressRanges(AddressRanges addressRanges) {
        this.addressRanges = addressRanges;
    }

}
