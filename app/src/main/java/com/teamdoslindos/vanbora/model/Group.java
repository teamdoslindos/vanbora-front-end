package com.teamdoslindos.vanbora.model;

import android.util.Log;

import java.util.List;

public class Group {

    private Integer id;

    private User driver;

    private String name;

    private String arrivalTime;

    private String returnTime;

    private String optimalStart;

    private String optimalEnd;

    private String description;

    private Localization address;

    private boolean active;

    private List<Passenger> passengers;

    public Group() {
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Localization getAddress() {
        return address;
    }

    public void setAddress(Localization address) {
        this.address = address;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public String getOptimalStart() {
        return optimalStart;
    }

    public void setOptimalStart(String optimalStart) {
        this.optimalStart = optimalStart;
    }

    public String getOptimalEnd() {
        return optimalEnd;
    }

    public void setOptimalEnd(String optimalEnd) {
        this.optimalEnd = optimalEnd;
    }

    @Override
    public String toString() {
        return name +
            "\n Viagem Ativa: " + (isActive() ? "Sim" : "Não") +
            "\n Endereço: " + address.getAddress().getStreetName() +
            ", " + address.getAddress().getStreetNumber() +
            " - " + address.getAddress().getMunicipalitySubdivision() +
            ", " + address.getAddress().getMunicipality();
    }

    public String toStringPassenger(Integer passengerId) {
        Passenger currentPassenger = null;
        for (Passenger passenger : passengers) {
            if (passenger.getUser().getId() == passengerId) {
                currentPassenger = passenger;
            }
        }
        return name +
                "\nViagem Ativa: " + (isActive() ? "Sim" : "Não") +
                "\nEndereço: " + address.getAddress().getStreetName() +
                ", " + address.getAddress().getStreetNumber() +
                " - " + address.getAddress().getMunicipalitySubdivision() +
                ", " + address.getAddress().getMunicipality() +
                "\n\nEmbarque" +
                "\nEmbarque confirmado: " +
                (currentPassenger.isDepartureConfirmed() ? "Sim" : "Não") +
                "\nHorário de embarque: " +
                (currentPassenger.getDepartureTime() == null ? currentPassenger.getOptimalDeparture() : currentPassenger.getDepartureTime()) +
                "\nChegada: " + arrivalTime +
                "\n\nRetorno" +
                "\nRetorno confirmada: " +
                (currentPassenger.isReturnConfirmed() ? "Sim" : "Não") +
                "\nSaída: " + returnTime +
                "\nHorário de retorno: " +
                (currentPassenger.getReturnTime() == null ? currentPassenger.getOptimalReturn() : currentPassenger.getReturnTime());

    }
}
