package com.teamdoslindos.vanbora.model;

public class Position {

    private Double lat;
    private Double lon;

    public Position() {
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
