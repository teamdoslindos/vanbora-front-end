package com.teamdoslindos.vanbora.model;

import java.util.Date;

public class User {

    private Integer id;

    private String name;

    private String email;

    private String password;

    private Date birthDate = new Date();

    private String phone;

    private String cpf;

    private String cnh;

    private Localization address;

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCnh() {
        return cnh;
    }

    public void setCnh(String cnh) {
        this.cnh = cnh;
    }

    public Localization getAddress() {
        return address;
    }

    public void setAddress(Localization address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Nome: '" + name + '\n' +
                "Phone:" + phone;
    }
}
