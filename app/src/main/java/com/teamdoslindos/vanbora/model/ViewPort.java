package com.teamdoslindos.vanbora.model;

public class ViewPort {

    private Position topLeftPoint;
    private Position btmRightPoint;

    public ViewPort() {
    }

    public Position getTopLeftPoint() {
        return topLeftPoint;
    }

    public void setTopLeftPoint(Position topLeftPoint) {
        this.topLeftPoint = topLeftPoint;
    }

    public Position getBtmRightPoint() {
        return btmRightPoint;
    }

    public void setBtmRightPoint(Position btmRightPoint) {
        this.btmRightPoint = btmRightPoint;
    }
}
