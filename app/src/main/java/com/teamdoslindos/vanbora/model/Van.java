package com.teamdoslindos.vanbora.model;

public class Van {

    private Integer id;
    private User driver;
    private String plate;
    private String color;
    private Integer quantityOfSeats;
    private String model;
    private byte[] photo;

    public Van(Integer id, String plate, String color, Integer quantityOfSeats, String model) {
        this.id = id;
        this.plate = plate;
        this.color = color;
        this.quantityOfSeats = quantityOfSeats;
        this.model = model;
    }

    public Van() {

    }


    public Integer getId() {
        return id;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        // Setter protected against null values to simplify business logic.
        if (driver != null) {
            this.driver = driver;
        }
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        // Setter protected against null values to simplify business logic.
        if (plate != null) {
            this.plate = plate;
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        // Setter protected against null values to simplify business logic.
        if (color != null) {
            this.color = color;
        }
    }

    public Integer getQuantityOfSeats() {
        return quantityOfSeats;
    }

    public void setQuantityOfSeats(Integer quantityOfSeats) {
        // Setter protected against null values to simplify business logic.
        if (quantityOfSeats != null) {
            this.quantityOfSeats = quantityOfSeats;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        // Setter protected against null values to simplify business logic.
        if (model != null) {
            this.model = model;
        }
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        // Setter protected against null values to simplify business logic.
        if (photo != null) {
            this.photo = photo;
        }
    }

    @Override
    public String toString() {
        return "Placa: " + plate + '\n' +
                "Cor: " + color + '\n' +
                "Modelo: " + model + '\n';
    }
}
