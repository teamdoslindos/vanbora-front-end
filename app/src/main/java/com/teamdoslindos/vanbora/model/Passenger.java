package com.teamdoslindos.vanbora.model;

public class Passenger {

    private User user;

    private String departureTime;

    private boolean departureConfirmed;

    private boolean returnConfirmed;

    private String returnTime;

    private String optimalDeparture;

    private String optimalReturn;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public boolean isDepartureConfirmed() {
        return departureConfirmed;
    }

    public void setDepartureConfirmed(boolean departureConfirmed) {
        this.departureConfirmed = departureConfirmed;
    }

    public boolean isReturnConfirmed() {
        return returnConfirmed;
    }

    public void setReturnConfirmed(boolean returnConfirmed) {
        this.returnConfirmed = returnConfirmed;
    }

    public String getOptimalDeparture() {
        return optimalDeparture;
    }

    public void setOptimalDeparture(String optimalDeparture) {
        this.optimalDeparture = optimalDeparture;
    }

    public String getOptimalReturn() {
        return optimalReturn;
    }

    public void setOptimalReturn(String optimalReturn) {
        this.optimalReturn = optimalReturn;
    }

    @Override
    public String toString() {
        return user.getName() +
                "\nEndereço: " +
                user.getAddress().getAddress().getStreetName() + ", " +
                user.getAddress().getAddress().getStreetNumber() +
                "\nHorário de embarque: " +
                (departureTime == null ? optimalDeparture : departureTime) +
                "\nHorário de retorno: " +
                (departureTime == null ? optimalReturn : returnTime) +
                "\nEmbarque confirmado: " +
                (departureConfirmed ? "Sim" : "Não") +
                "\nRetorno confirmada: " +
                (returnConfirmed ? "Sim" : "Não");
    }
}
