package com.teamdoslindos.vanbora.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.controller.AddressController;
import com.teamdoslindos.vanbora.controller.CallBackAddress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class GroupRegisterActivity extends AppCompatActivity {

    private EditText uEditName;
    private EditText uEditHoraSaida;
    private EditText uEditHoraRetorno;
    private EditText uEditDesc;
    private EditText uEditRua;
    private EditText uEditNum;
    private EditText uEditBairro;
    private EditText uEditCidade;
    private AddressController addressController = new AddressController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_group);
        Button buttonParse = findViewById(R.id.registerButton);

        RequestQueue queue = Volley.newRequestQueue(this);
        Button button = findViewById(R.id.cancelButton);
        setSendToLogin(button);

        buttonParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uEditName = findViewById(R.id.editName);
                uEditHoraSaida = findViewById(R.id.editHoraSaida);
                uEditHoraRetorno = findViewById(R.id.editHoraRetorno);
                uEditDesc = findViewById(R.id.editDesc);
                uEditRua = findViewById(R.id.editRua);
                uEditNum = findViewById(R.id.editNum);
                uEditBairro = findViewById(R.id.editBairro);
                uEditCidade = findViewById(R.id.editCidade);

                try {
                    String value = uEditRua.getText().toString() + " "
                            + uEditNum.getText().toString() + " "
                            + uEditBairro.getText().toString() + " "
                            + uEditCidade.getText().toString();

                    addressController.getAddressFromTomTom(GroupRegisterActivity.this, value,
                            new CallBackAddress() {
                                @Override
                                public void callBackAddress(JSONObject jsonObject) {
                                    Log.d("Endereço", jsonObject.toString());

                                    JSONArray jsonArray = new JSONArray();
                                    //Cria um JSONObject pra cada item
                                    JSONObject jsonObjectGroup = new JSONObject();
                                    JSONArray jsonPassageiros = new JSONArray();
                                    JSONObject jsonObjectDriver = new JSONObject();
                                    try {
                                        //Constroi o JSONObject
                                        jsonObjectDriver.put("id", LoginActivity.user.getId());
                                        jsonObjectGroup.put("name", uEditName.getText());
                                        jsonObjectGroup.put("arrivalTime", uEditHoraSaida.getText());
                                        jsonObjectGroup.put("returnTime", uEditHoraRetorno.getText());
                                        jsonObjectGroup.put("description", uEditDesc.getText());
                                        jsonObjectGroup.put("driver", jsonObjectDriver);
                                        jsonObjectGroup.put("passengers", jsonPassageiros);
                                        jsonObjectGroup.put("address", jsonObject);

                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                    Log.d("Log", jsonObjectGroup.toString());

                                    //POST Request
                                    final String mRequestBody = jsonObjectGroup.toString();
                                    String url = ConfigUtils.API_HEROKU_BACK_END + "group";
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Log.i("LOG_VOLLEY", response);
                                            Snackbar.make(findViewById(R.id.telaGroupRegister), "Cadastrado com sucesso!",
                                                    Snackbar.LENGTH_SHORT)
                                                    .show();
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("LOG_VOLLEY", error.toString());
                                            Snackbar.make(findViewById(R.id.telaGroupRegister), "Algo deu errado! Tente novamente.",
                                                    Snackbar.LENGTH_SHORT)
                                                    .show();
                                        }
                                    }) {
                                        @Override
                                        public String getBodyContentType() {
                                            return "application/json; charset=utf-8";
                                        }

                                        @Override
                                        public byte[] getBody() throws AuthFailureError {
                                            Log.d("Log", jsonObjectGroup.toString());
                                            return mRequestBody == null ? null : mRequestBody.getBytes(StandardCharsets.UTF_8);
                                        }

                                        @Override
                                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                                            String responseString = "";
                                            if (response != null) {

                                                responseString = String.valueOf(response.statusCode);

                                            }
                                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                                        }
                                    };

                                    queue.add(stringRequest);

                                    //Adiciona-o ao JSONArray
                                    jsonArray.put(jsonObjectGroup);
                                }
                            });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setSendToLogin(Button button) {
        Intent intent = new Intent(this, LoginActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }


}
