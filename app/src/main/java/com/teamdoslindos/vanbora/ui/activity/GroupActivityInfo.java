package com.teamdoslindos.vanbora.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.GroupInfoController;

public class GroupActivityInfo extends AppCompatActivity {

    private GroupInfoController groupInfoController = new GroupInfoController();
    private TextView mCode;
    private TextView mDestiny;
    private TextView mAddress;
    private TextView mDate;
    private TextView mIsGroupActive;

    private LinearLayout mTelaGroupInfo;

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_info);

        mTelaGroupInfo = findViewById(R.id.telaGroupInfo);
        mCode = findViewById(R.id.tripCode);
        mDestiny = findViewById(R.id.groupDestiny);
        mAddress = findViewById(R.id.groupAddress);
        mDate = findViewById(R.id.groupDate);
        mIsGroupActive = findViewById(R.id.isGroupActive);

        groupInfoController.getGroupInfo(mTelaGroupInfo, mCode, mDestiny, mAddress, mDate,
                mIsGroupActive, GroupActivity.group.getId(), GroupActivityInfo.this);
    }
}
