package com.teamdoslindos.vanbora.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.teamdoslindos.vanbora.R;

public class ChooseRegisterActivity extends Activity {

    private Button btnDriver;
    private Button btnPassenger;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_register);
        btnDriver = findViewById(R.id.btnToDriver);
        btnPassenger = findViewById(R.id.btnToPassenger);

        setSendToDriverRegister(btnDriver);
        setSendToPassengerRegister(btnPassenger);

        btnDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSendToDriverRegister(btnDriver);
            }
        });

        btnPassenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSendToPassengerRegister(btnPassenger);
            }
        });

    }

    private void setSendToPassengerRegister(Button button) {
        Intent intent = new Intent(this, RegisterPassengerActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }

    private void setSendToDriverRegister(Button button) {
        Intent intent = new Intent(this, RegisterDriverActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }
}
