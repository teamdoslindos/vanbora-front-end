package com.teamdoslindos.vanbora.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.EditEstimatedHoursController;

import org.json.JSONException;
import org.json.JSONObject;

public class EditEstimatedHoursActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private EditText mEditReturnConfirmedEstimated;
    private EditText mEditDepartureTimeEstimated;
    private ArrayAdapter<String> adapter;
    private LinearLayout mTelaEditHoursEstimated;
    private Button mSaveHoursEstimated;
    private Spinner mSpinnerPassenger;
    private Integer position;
    private String mRequestBody;
    private Integer passengerId;
    private EditEstimatedHoursController editEstimatedHoursController = new EditEstimatedHoursController();

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_edit_estimated_hours);
        mSaveHoursEstimated = findViewById(R.id.editEstimatedHours);
        mTelaEditHoursEstimated = findViewById(R.id.telaEditEstimatedHours);
        mSpinnerPassenger = findViewById(R.id.spinnerPassenger);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, GroupActivity.passengersName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerPassenger.setAdapter(adapter);
        mSpinnerPassenger.setOnItemSelectedListener(this);

        mSaveHoursEstimated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObject = new JSONObject();
                String time = "";
                mEditReturnConfirmedEstimated = findViewById(R.id.editReturnEstimated);
                mEditDepartureTimeEstimated = findViewById(R.id.editDepartureEstimated);
                if (!mEditReturnConfirmedEstimated.getText().toString().isEmpty()) {
                    time = mEditReturnConfirmedEstimated.getText().toString();
                    try {
                        jsonObject.put("time", time);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mRequestBody = jsonObject.toString();
                    editEstimatedHoursController.changeHoursEstimatedOfPassenger(mTelaEditHoursEstimated,
                            passengerId, mRequestBody, EditEstimatedHoursActivity.this,
                            false, true);
                }
                if (!mEditDepartureTimeEstimated.getText().toString().isEmpty()) {
                    time = mEditDepartureTimeEstimated.getText().toString();
                    try {
                        jsonObject.put("time", time);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mRequestBody = jsonObject.toString();
                    editEstimatedHoursController.changeHoursEstimatedOfPassenger(mTelaEditHoursEstimated,
                            passengerId, mRequestBody, EditEstimatedHoursActivity.this,
                            true, false);
                }
                adapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        position = i;
        passengerId = getPassengerId(i);
    }

    private Integer getPassengerId(int i) {
        return GroupActivity.passengers.get(i).getUser().getId();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
