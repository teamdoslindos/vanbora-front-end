package com.teamdoslindos.vanbora.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.Nullable;

import com.teamdoslindos.vanbora.R;

public class ChooseGroupActivity extends Activity implements AdapterView.OnItemSelectedListener{

    private ArrayAdapter<String> adapter;
    private Button mSeeTripDetails;
    private Spinner mSpinnerGroups;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_group);
        mSeeTripDetails = findViewById(R.id.seeTripDetails);
        mSpinnerGroups = findViewById(R.id.spinnerGroups);
        setSendToGroup(mSeeTripDetails);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, HomeDriverActivity.listGroupsNames);
        mSpinnerGroups.setAdapter(adapter);
        mSpinnerGroups.setOnItemSelectedListener(this);

        mSeeTripDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSendToGroup(mSeeTripDetails);
            }
        });

    }

    private void setSendToGroup(Button button) {
        Intent intent = new Intent(this, GroupActivity.class);
        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        GroupActivity.setGroup(HomeDriverActivity.listGroups.get(i));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
