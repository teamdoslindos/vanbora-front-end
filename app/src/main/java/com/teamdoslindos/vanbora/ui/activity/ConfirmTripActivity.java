package com.teamdoslindos.vanbora.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.ConfirmTripController;

public class ConfirmTripActivity extends Activity implements AdapterView.OnItemSelectedListener {

    private ArrayAdapter<String> adapter;
    private Button mChangeStatus;
    private Spinner mSpinnerTrips;
    private RadioGroup typeOfTrip;
    private RadioButton radioButton;
    private LinearLayout mTelaConfirmTrip;
    private Integer position;
    private Integer tripId;
    private ConfirmTripController confirmTripController = new ConfirmTripController();

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger_confirm_travel);
        mChangeStatus = findViewById(R.id.confirmStatus);
        mSpinnerTrips = findViewById(R.id.spinnerTrips);
        mTelaConfirmTrip = findViewById(R.id.telaConfirmTrip);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, HomePassengerActivity.tripsNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerTrips.setAdapter(adapter);
        mSpinnerTrips.setOnItemSelectedListener(this);
        typeOfTrip = findViewById(R.id.typeOfTrip);

        mChangeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = typeOfTrip.getCheckedRadioButtonId();
                radioButton = findViewById(selectedId);
                String typeOfTrip = (String) radioButton.getText();
                switch (typeOfTrip) {
                    case "Ida":
                        confirmTripController.changeTripDepartureStatus(true, tripId,
                                mTelaConfirmTrip, ConfirmTripActivity.this);
                        break;
                    case "Volta":
                        confirmTripController.changeTripDepartureStatus(false, tripId,
                                mTelaConfirmTrip, ConfirmTripActivity.this);
                        break;
                }
                adapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        position = i;
        tripId = getTripId(i);
    }

    private Integer getTripId(int i) {
        return HomePassengerActivity.tripsList.get(i).getId();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
