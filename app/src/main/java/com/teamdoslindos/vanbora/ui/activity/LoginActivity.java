package com.teamdoslindos.vanbora.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.CallBackLogin;
import com.teamdoslindos.vanbora.controller.LoginController;
import com.teamdoslindos.vanbora.model.User;

import org.json.JSONException;

public class LoginActivity extends Activity {

    public static User user = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button loggingButton = findViewById(R.id.btnLoginLogar);
        Button registerButton = findViewById(R.id.btnLoginCatastrar);

        TextView emailAddress = findViewById(R.id.editTextTextEmailAddress);
        TextView password = findViewById(R.id.editTextTextPassword);

        setLoginListener(loggingButton, emailAddress, password);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSendToChooseRegister(registerButton);
            }
        });

    }

    private void setLoginListener(Button loggingButton, TextView emailAddress, TextView password) {
        Intent intentDriver = new Intent(this, HomeDriverActivity.class);
        Intent intentPassenger = new Intent(this, HomePassengerActivity.class);

        Toast toast = Toast.makeText(
                this,
                "Email ou senha errados...",
                Toast.LENGTH_SHORT);

        loggingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LoginController loginController = new LoginController();

                try {
                    loginController.postUserLogin(
                            getApplicationContext(),
                            emailAddress.getText().toString(),
                            password.getText().toString(),
                            new CallBackLogin() {

                                @Override
                                public void callBack(User user) {
                                    if (user == null) {
                                        toast.show();
                                    } else if (user.getCnh() == null) {
                                        LoginActivity.user = user;
                                        startActivity(intentPassenger);
                                    } else {
                                        LoginActivity.user = user;
                                        startActivity(intentDriver);
                                    }
                                }
                            }
                    );
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void setSendToChooseRegister(Button button) {
        Intent intent = new Intent(this, ChooseRegisterActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }

}

