package com.teamdoslindos.vanbora.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.GroupController;
import com.teamdoslindos.vanbora.model.Group;
import com.teamdoslindos.vanbora.model.Passenger;
import com.teamdoslindos.vanbora.model.User;

import java.util.ArrayList;

public class GroupActivity extends AppCompatActivity {

    public static ArrayList<Passenger> allPassengers = new ArrayList<>();
    private boolean showingAllPassengers = false;
    public static ArrayList<Passenger> passengers = new ArrayList<>();
    private ArrayAdapter<Passenger> adapter;
    public static ArrayList<String> passengersName = new ArrayList<String>();
    public static Group group;
    private GroupController groupController = new GroupController();
    private LinearLayout mTelaGroup;
    private TextView mGroupInfo;
    private ListView listViewPassenger;
    private Button buttonInfo;
    private Button buttonEdit;
    private Button buttonStartTrip;
    private Button btnGetPassengers;

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        listViewPassenger = findViewById(R.id.passengerList);
        adapter = new ArrayAdapter<Passenger>(this,
                R.layout.list_trip,
                allPassengers);
        listViewPassenger.setAdapter(adapter);
        mTelaGroup = findViewById(R.id.telaGroup);
        mGroupInfo = findViewById(R.id.groupVan);
        groupController.getGroupInfo(mGroupInfo, group.getId(), GroupActivity.this);

        buttonInfo = findViewById(R.id.infoView);
        setSendToInfo(buttonInfo);

        buttonEdit = findViewById(R.id.editEstimatedHour);
        setSendToEditHours(buttonEdit);

        buttonStartTrip = findViewById(R.id.beginTrip);
        if (group != null && group.isActive()) {
            buttonStartTrip.setText("Encerrar viagem");
        }
        groupController.setToggleTrip(mTelaGroup,
                buttonStartTrip, GroupActivity.this);

        btnGetPassengers = findViewById(R.id.passageirosView);
        adapter.notifyDataSetChanged();

        btnGetPassengers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                togglePassengersView(btnGetPassengers);
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void setSendToInfo(Button button) {
        Intent intent = new Intent(this, GroupActivityInfo.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }

    public void setSendToEditHours(Button button) {
        Intent intent = new Intent(this, EditEstimatedHoursActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }

    public void togglePassengersView(Button button) {
        groupController.getPassengersFromGroup(mTelaGroup, adapter,
                group.getId(), GroupActivity.this);
        adapter.notifyDataSetChanged();
        if (showingAllPassengers) {
            passengers = new ArrayList<>();
            for (Passenger passenger : allPassengers) {
                if (passenger.isDepartureConfirmed() || passenger.isReturnConfirmed()) {
                    passengers.add(passenger);
                }
            }
            button.setText("Ver todos");
        } else {
            passengers = allPassengers;
            button.setText("Ver confirmados");
        }
        showingAllPassengers = !showingAllPassengers;
        adapter.notifyDataSetChanged();
    }

    public static void setGroup(Group group) {
        GroupActivity.group = group;
    }
}
