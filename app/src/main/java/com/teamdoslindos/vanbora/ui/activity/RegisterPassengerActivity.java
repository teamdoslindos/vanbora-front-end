package com.teamdoslindos.vanbora.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.config.ConfigUtils;
import com.teamdoslindos.vanbora.controller.AddressController;
import com.teamdoslindos.vanbora.controller.CallBackAddress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class RegisterPassengerActivity extends AppCompatActivity {

    private EditText uEditName;
    private EditText uEditCPF;
    private EditText uEditDataNasc;
    private EditText uEditTel;
    private EditText uEditEmail;
    private EditText uEditPassword;
    private EditText mStreetText;
    private EditText mNumberText;
    private EditText mCityStreet;
    private Button buttonRegister;
    private Button buttonCancel;
    private RequestQueue queue;
    private AddressController addressController = new AddressController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_passenger);
        buttonRegister = findViewById(R.id.registerButton);
        uEditName = findViewById(R.id.editName);
        uEditCPF = findViewById(R.id.editCpf);
        uEditDataNasc = findViewById(R.id.editDataNasc);
        uEditTel = findViewById(R.id.editTel);
        uEditEmail = findViewById(R.id.editEmail);
        uEditPassword = findViewById(R.id.editPassword);


        queue = Volley.newRequestQueue(this);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONArray jsonArray = new JSONArray();
                //Cria um JSONObject pra cada item
                JSONObject jsonObjectPessoal = new JSONObject();

                // Busca o endereço no tomtom
                mStreetText = findViewById(R.id.street_edit_text);
                mNumberText = findViewById(R.id.number_edit_text);
                mCityStreet = findViewById(R.id.city_edit_text);
                String value = mStreetText.getText().toString() + " "
                        + mNumberText.getText().toString() + " "
                        + mCityStreet.getText().toString();


                try {
                    addressController.getAddressFromTomTom(RegisterPassengerActivity.this, value,
                            new CallBackAddress() {
                                @Override
                                public void callBackAddress(JSONObject jsonObject) {
                                    try {
                                        //Constroi o JSONObject
                                        jsonObjectPessoal.put("name", uEditName.getText());
                                        jsonObjectPessoal.put("email", uEditEmail.getText());
                                        jsonObjectPessoal.put("password", uEditPassword.getText());
                                        jsonObjectPessoal.put("birthDate", uEditDataNasc.getText());
                                        jsonObjectPessoal.put("phone", uEditTel.getText());
                                        jsonObjectPessoal.put("cpf", uEditCPF.getText());
                                        jsonObjectPessoal.put("address", jsonObject);

                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    //POST Request
                                    final String mRequestBody = jsonObjectPessoal.toString();
                                    Log.d("Log", mRequestBody);

                                    String url = ConfigUtils.API_HEROKU_BACK_END + "user";
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Log.i("LOG_VOLLEY", response);
                                            Log.e("response", response);
                                            Snackbar.make(findViewById(R.id.telaRegister), "Cadastrado com sucesso!",
                                                    Snackbar.LENGTH_SHORT).setActionTextColor(Color.GREEN)
                                                    .show();
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("LOG_VOLLEY", error.toString());
                                            Snackbar.make(findViewById(R.id.telaRegister), "Algo deu errado! Por favor tente novamente.",
                                                    Snackbar.LENGTH_SHORT).setActionTextColor(Color.RED)
                                                    .show();
                                        }
                                    }) {
                                        @Override
                                        public String getBodyContentType() {
                                            return "application/json; charset=utf-8";
                                        }

                                        @Override
                                        public byte[] getBody() throws AuthFailureError {
                                            return mRequestBody == null ? null : mRequestBody.getBytes(StandardCharsets.UTF_8);
                                        }

                                        @Override
                                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                                            String responseString = "";
                                            if (response != null) {
                                                responseString = String.valueOf(response.statusCode);
                                            }
                                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                                        }
                                    };

                                    queue.add(stringRequest);

                                    //Adiciona-o ao JSONArray
                                    jsonArray.put(jsonObjectPessoal);
                                    Log.d("Array", jsonArray.toString());
                                }
                            });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        buttonCancel = findViewById(R.id.cancelButton);
        setSendToLogin(buttonCancel);
    }

    public void setSendToLogin(Button button) {
        Intent intent = new Intent(this, LoginActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }
}
