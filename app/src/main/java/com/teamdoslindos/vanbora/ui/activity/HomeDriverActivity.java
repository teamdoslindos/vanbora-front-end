package com.teamdoslindos.vanbora.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.HomeDriverController;
import com.teamdoslindos.vanbora.model.Group;
import com.teamdoslindos.vanbora.model.Van;

import java.util.ArrayList;

public class HomeDriverActivity extends AppCompatActivity {

    public static ArrayList<Group> listGroups = new ArrayList<>();
    public static ArrayList<String> listGroupsNames = new ArrayList<>();
    private ArrayAdapter<Group> adapterGroup;

    public static ArrayList<Van> listVans = new ArrayList<>();
    private ArrayAdapter<Van> adapterVan;

    private HomeDriverController homeDriverController = new HomeDriverController();

    private RelativeLayout mTelaHomeDriver;
    private ListView listTripView;
    private ListView listVanView;
    private Button mGetVanList;
    private Button mGetTripList;
    private Button mAddTrip;
    private Button mAddVan;
    private Button mTripDetail;

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_home_driver);

        listTripView = findViewById(R.id.tripListDriver);
        adapterGroup = new ArrayAdapter<Group>(this,
                R.layout.list_trip,
                listGroups);
        listTripView.setAdapter(adapterGroup);
        adapterGroup.notifyDataSetChanged();

        listVanView = findViewById(R.id.vanList);
        adapterVan = new ArrayAdapter<Van>(this,
                R.layout.list_van,
                listVans);
        listVanView.setAdapter(adapterVan);
        adapterVan.notifyDataSetChanged();

        mTelaHomeDriver = findViewById(R.id.telaHomeDriver);
        mGetVanList = findViewById(R.id.getVanList);
        mGetTripList = findViewById(R.id.getTripList);
        mAddTrip = findViewById(R.id.addGroupView);
        mAddVan = findViewById(R.id.addVanView);
        mTripDetail = findViewById(R.id.getDetailTrips);

        setSendToRegisterVan(mAddVan);
        setSendToRegisterGroup(mAddTrip);
        setSendToChooseGroup(mTripDetail);

        mGetTripList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeDriverController.getTripsFromDriver(mTelaHomeDriver, HomeDriverActivity.this);
                adapterGroup.notifyDataSetChanged();
            }
        });

        mGetVanList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeDriverController.getVansFromDriver(mTelaHomeDriver, HomeDriverActivity.this);
                adapterVan.notifyDataSetChanged();
            }
        });
    }

    private void setSendToRegisterVan(Button button) {
        Intent intent = new Intent(this, VanRegisterActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }

    private void setSendToRegisterGroup(Button button) {
        Intent intent = new Intent(this, GroupRegisterActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }

    private void setSendToChooseGroup(Button button) {
        Intent intent = new Intent(this, ChooseGroupActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }
}
