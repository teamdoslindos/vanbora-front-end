package com.teamdoslindos.vanbora.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.VanRegisterController;
import com.teamdoslindos.vanbora.model.Van;

public class VanRegisterActivity extends AppCompatActivity {

    private LinearLayout mTelaVanRegister;
    private EditText placaField;
    private EditText colorField;
    private EditText qtdSeatsField;
    private EditText fabricanteField;
    private Button btnCancel;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_van_register);

        mTelaVanRegister = findViewById(R.id.telaVanRegister);
        placaField = findViewById(R.id.placa);
        colorField = findViewById(R.id.color);
        qtdSeatsField = findViewById(R.id.qtdSeats);
        fabricanteField = findViewById(R.id.fabricante);

        btnCancel = findViewById(R.id.cancel);
        setSendToHomeDriver(btnCancel);

        btnSubmit = findViewById(R.id.submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Van van = new Van(
                        null,
                        placaField.getText().toString(),
                        colorField.getText().toString(),
                        Integer.parseInt(qtdSeatsField.getText().toString()),
                        fabricanteField.getText().toString()
                );
                VanRegisterController.updateVan(mTelaVanRegister, VanRegisterActivity.this, van);
            }
        });
    }

    public void setSendToHomeDriver(Button button) {
        Intent intent = new Intent(this, HomeDriverActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }
}
