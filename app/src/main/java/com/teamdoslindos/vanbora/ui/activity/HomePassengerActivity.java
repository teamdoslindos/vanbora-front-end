package com.teamdoslindos.vanbora.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.teamdoslindos.vanbora.R;
import com.teamdoslindos.vanbora.controller.HomePassengerController;
import com.teamdoslindos.vanbora.model.Group;

import org.json.JSONException;

import java.util.ArrayList;

public class HomePassengerActivity extends AppCompatActivity {

    public static ArrayList<Group> tripsList = new ArrayList<>();
    public static ArrayList<String> tripsListString = new ArrayList<>();
    public static ArrayList<String> tripsNames = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private LinearLayout mTelaHomePassenger;
    private ListView listViewTrips;
    private EditText tripCodeField;
    private Button btnAddTrip;
    private Button btnRefreshTrips;

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_home_passenger);
        listViewTrips = findViewById(R.id.tripList);
        adapter = new ArrayAdapter<String>(this,
                R.layout.list_trip,
                tripsListString);
        listViewTrips.setAdapter(adapter);
        mTelaHomePassenger = findViewById(R.id.telaHomePassenger);

        tripCodeField = findViewById(R.id.tripCode);
        btnAddTrip = findViewById(R.id.addTrip);
        btnRefreshTrips = findViewById(R.id.refreshTrips);
        adapter.notifyDataSetChanged();

        Button btnConfirmTrip = findViewById(R.id.confirmPresence);
        setSendToConfirmTrip(btnConfirmTrip);

        btnAddTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tripCode = tripCodeField.getText().toString();
                try {
                    HomePassengerController.addPassengerToGroup(mTelaHomePassenger, HomePassengerActivity.this, tripCode);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        btnRefreshTrips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomePassengerController.getTripsFromPassenger(mTelaHomePassenger, HomePassengerActivity.this);
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void setSendToConfirmTrip(Button button) {
        Intent intent = new Intent(this, ConfirmTripActivity.class);

        button.setOnClickListener(view -> {
            startActivity(intent);
        });
    }
}
